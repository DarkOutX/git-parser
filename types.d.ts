
import {COMMIT_TYPE, REPORT_TYPE, TEXT_STYLE} from './consts';

// copy from node_modules/git-parse/dist/types/git_commit_type.js.flow
export namespace IGitParser {
    export type FileModification = {
        path: string,
        linesAdded?: number,
        linesDeleted?: number,
    };

    export type FileRename = {
        oldPath: string,
        newPath: string,
    };

    export type GitCommit = {
        hash: string,
        authorName: string,
        authorEmail: string,
        date: string,
        message: string,
        filesAdded: FileModification[],
        filesDeleted: FileModification[],
        filesModified: FileModification[],
        filesRenamed: FileRename[],
    };
}

export interface IOptions {
    from?: number;
    to?: number;
    isNeedAuthors?: boolean;
    skipTypes?: Partial<Record<COMMIT_TYPE, boolean>>;
}

type TMessagesByModules = Record<string, string[]>;

export type TMessages = Partial<Record<COMMIT_TYPE, TMessagesByModules>>;
export interface IMessageParsed {
    type: COMMIT_TYPE;
    module: string;
    text: string;
}

type TTextStyle = Record<TEXT_STYLE | COMMIT_TYPE, {pre: string, post: string}>;

export type TTextStyles = Record<REPORT_TYPE, TTextStyle>;
