
import type { IHook } from "./pre-push";
import { parseCommitMsg } from './index';
import { COMMIT_TYPE } from "consts";

export interface FormatHookMessageOptions {
    /** Adds tags at the end of message */
    isAddTags?: boolean;
    /** Adds branch tags */
    isAddBranchTag?: boolean;
    /** Detect task tag from commit message */
    autoTag_task?: boolean;
    /** Detect tags by searching words in file paths */
    autoTagsMap_files?: Record<string /* word to search */, string /* tag */>;
}

export function formatHookMessage(commitData: IHook.CommitData, options: FormatHookMessageOptions): string {
    const tags: string[] = [];
    const {
        message: _commitMsg,
        branchName,
        changes,
        timestamp,
        user,
    } = commitData;
    const {
        autoTag_task,
        autoTagsMap_files,
        isAddTags,
    } = options;
    const {
        type,
        module,
        text: commitMsg,
    } = parseCommitMsg(_commitMsg);

    if (isAddTags) {
        const tagsAddedMap: Record<string, boolean> = {};

        tags.push(type);
        tagsAddedMap[type] = true;
        tags.push(module);
        tagsAddedMap[module] = true;

        if (autoTagsMap_files) {
            const wordsToSearch = Object.keys(autoTagsMap_files);
            const filesChanged = Object.keys(changes);

            for (const filePath of filesChanged) {
                for (const wordToSearch of wordsToSearch) {
                    if (filePath.includes(wordToSearch)) {
                        const tagToAdd = autoTagsMap_files[wordToSearch];

                        if (!tagsAddedMap[tagToAdd]) {
                            tagsAddedMap[tagToAdd] = true;
                            tags.push(tagToAdd);
                        }
                    }
                }
            }
        }

        const i_taskTag = commitMsg.indexOf('#');

        if (!tagsAddedMap[branchName]) {
            tags.push(branchName);
            tagsAddedMap[branchName] = true;
        }

        if (autoTag_task && i_taskTag) {
            const taskId = commitMsg.slice(i_taskTag + 1);

            tags.push(`task-${taskId}`);
        }
    }

    let commitTypeText = `${type}`;

    switch (type) {
        case COMMIT_TYPE.DEPENDENCIES: {
            commitTypeText = 'Обновление зависимостей'; break;
        }
        case COMMIT_TYPE.DOC: {
            commitTypeText = 'Документация'; break;
        }
        case COMMIT_TYPE.FEATURE: {
            commitTypeText = 'Новый функционал'; break;
        }
        case COMMIT_TYPE.FIX: {
            commitTypeText = 'Исправление'; break;
        }
        case COMMIT_TYPE.REFACTORING: {
            commitTypeText = 'Рефакторинг'; break;
        }
        case COMMIT_TYPE.TEST: {
            commitTypeText = 'Тестирование'; break;
        }
    }

    return `**${commitTypeText}**\n${commitMsg}\n\n#${tags.join(', #')}`;
}