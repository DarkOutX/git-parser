#!/usr/bin/env node

import type {IHook} from './pre-push';

/**
 *
 * This code will ensure that before every commit in your client repository, your branch name and commit message adheres to a certain contract.
 * In this example, branch names must be like 'feature/AP-22-some-feature-name' or 'hotfix/AP-30-invitation-email-not-sending', and commit messages must start like some issue code, like AP-100 or AP-101.
 *
 * 'AP' just stands for Acme Platform and is an example. I made this example with Jira in mind, since Jira issue codes have this structure -as far as I know-, but is pretty easy to change it to any other issue id like #100 or $-AP-120, or whatever.
 *
 * In order for this to work, you should go to .git/hooks in any git client repository and create a commit-msg file (or modify the provided by default, commit-msg.sample) with
 * this contents.
 *
 * You need an await/async compliant version of node installed in your machine.
 *
 * Don't forget to make your file executable!
 */

const OPTIONS: IHook.Options = {
    isCheckBranchName: false,
    branchRegEx: /^(feature|hotfix)\/AP-[0-9]{1,6}-/,

    isCheckCommitMsg: false,
    commitMsgRegEx: /AP-[0-9]{1,6}-/,

    webhookOptions: {
        enabled: true,
        isGetFileChanges: true,
        isSendNewFileContent: false,
        hostname: 'localhost',
        port: 888,
        path: '/hook',
        //host: '10.19.10.17',
        //port: 40985,
        //path: '/callForceTelegramBot/ping',

        credentials: {
            login: '',
            password: '',
        },
        customHeaders: {

        },
    },

    timeout: 3000,
    isDebug: true,
};

const fs = require('fs');
const childProcessExec = require('child_process').exec;
const util = require('util');
const http = require('http');
const os = require('os');

const exec = util.promisify(childProcessExec);

handleCommit();
hookCleanup();

async function handleCommit() {
    const {
        isCheckBranchName,
        branchRegEx,

        isCheckCommitMsg,
        commitMsgRegEx,

        webhookOptions,
    } = OPTIONS;
    let branchName = '';

    try{
        branchName = await getCurrentBranch();
    }
    catch (err){
        handleGitBranchCommandError(err);
    }

    if (isCheckBranchName && branchRegEx && !branchRegEx.test(branchName)) {
        handleBadBranchName();
    }

    const newCommitsHashesAndMsgs = await getNewCommits(branchName);
    let commits: (IHook.CommitData_min | IHook.CommitData)[] = [];

    for (const [ commitHash, commitMsg ] of newCommitsHashesAndMsgs) {
        if (isCheckCommitMsg && commitMsgRegEx && !commitMsgRegEx.test(commitMsg)) {
            handleBadCommitMessage();
        }

        if (webhookOptions && webhookOptions.enabled) {
            const commitMin: IHook.CommitData_min = {
                hash: commitHash,
                message: commitMsg,
                branchName,
            };

            if (webhookOptions.isGetFileChanges) {
                const commitData = await _execGitCmd(`git show ${commitHash}`);
                const [ changes, changesAmount, date, name, mail ] = parseGitData(commitData);
                const commitFull: IHook.CommitData = {
                    ...commitMin,
                    date,
                    timestamp: new Date(date).getTime(),
                    user: {
                        name,
                        mail,
                    },
                    changes,
                    changesAmount,
                };

                commits.push(commitFull);
            }
            else {
                commits.push(commitMin);
            }
        }
    }

    if (webhookOptions && webhookOptions.enabled) {
        await fetch(commits);
    }

    if (OPTIONS.isDebug) {
        process.exit(1);
    }
    process.exit(0);
}

async function getNewCommits(branchName) {
    const commits = [];
    const commitsAmount = await getNewCommitsAmount(branchName);
    const allCommitsOutput = await _execGitCmd(`git log --pretty=oneline -n${commitsAmount}`);

    return allCommitsOutput.trim().split('\n').map(commitStr => [commitStr.slice(0, 40), commitStr.slice(41)]);
}

async function getCurrentBranch() {
    const branches = await _execGitCmd('git branch');
    const curBranch = branches.split('\n').find(b => b.trim().charAt(0) === '*' );

    if (!curBranch) {
        throw new Error(`[getCurrentBranch] Can't detect current branch`);
    }

    return curBranch.trim().substring(2);
}

async function getNewCommitsAmount(branchName) {
    const _localCommitsAmount = await _execGitCmd(`git rev-list HEAD --count`);
    const localCommitsAmount = parseInt(_localCommitsAmount.slice(0, -1), 10);

    const _remoteCommitsAmount = await _execGitCmd(`git rev-list origin/${branchName} --count`);
    const remoteCommitsAmount = parseInt(_remoteCommitsAmount.slice(0, -1), 10);

    return localCommitsAmount - remoteCommitsAmount;
}

function parseGitData(commitData): [
    changes: Record<string, IHook.FileChanges_strict>, changesAmount: number, date: string, name: string, mail: string,
] {
    const rows = commitData.split('\n');
    const filesChanges: Record<string, IHook.FileChanges_strict> = {};
    let changesCounter = 0;
    let curFileChanges: IHook.FileChanges | void = void 0;
    let date = '--unknown--';
    let name = '--unknown--';
    let mail = '--unknown--';

    for (let i = 0; i < rows.length; i++) {
        const row = rows[i];

        if (row.startsWith('Date:')) {
            date = row.replace('Date:', '').trim();
        }
        else if (row.startsWith('Author:')) {
            const nameAndMail = row.replace('Author:', '').trim();

            name = nameAndMail.replace(/(.+) <.+>/gi, '$1');
            mail = nameAndMail.replace(/.+ <(.+)>/gi, '$1');
        }
        // Found file changes info
        else if (row.startsWith('+++ b/') || row.startsWith('+++ /dev/null') || row.startsWith('rename from')) {
            const isMoved = row.startsWith('rename from');
            const isDeleted = row.startsWith('+++ /dev/null');
            const prevLine = rows[i - 1];
            let filename

            if (isDeleted) {
                filename = prevLine.replace('--- a/', '');
            }
            else if (isMoved) {
                filename = row.replace('rename from ', '').split('/').pop();
            }
            else {
                filename = row.replace('+++ b/', '');
            }

            if (filesChanges[filename]) {
                // Unreachable code, first file entrance
                throw new Error('Found file which is already parsed');
            }

            filesChanges[filename] = {};
            curFileChanges = filesChanges[filename];
            changesCounter++;

            const isNewFile = prevLine === '--- /dev/null';

            if (isNewFile) {
                (curFileChanges as IHook.FileChanges_newFile).isNewFile = true;
            }

            if (isMoved) {
                (curFileChanges as IHook.FileChanges_moved).isMoved = true;
                (curFileChanges as IHook.FileChanges_moved).oldPath = row.replace('rename from ', '');
            }

            if (isDeleted) {
                (curFileChanges as IHook.FileChanges_deleted).isDeletedFile = true;
            }

            continue;
        }
        // End of file changes info
        else if (row.startsWith('git diff')) {
            curFileChanges = void 0;

            continue;
        }
        else if (row.startsWith('rename to')) {
            if (!curFileChanges) {
                throw new Error(`[pre-push] found "rename to", but still not detected affected file`);
            }

            curFileChanges.newPath = row.replace('rename to ', '');
            curFileChanges = void 0;

            continue;
        }

        // Handling file changes info
        if (curFileChanges) {
            const isAdded = row.startsWith('+') && !row.startsWith('+++');
            const isRemoved = row.startsWith('-') && !row.startsWith('---');
            const {
                isNewFile,
                isDeletedFile,
            } = curFileChanges;

            if (isDeletedFile) {
                continue;
            }

            if (isAdded && (!isNewFile || (isNewFile && OPTIONS.webhookOptions?.isSendNewFileContent))) {
                if (!curFileChanges.added) {
                    curFileChanges.added = '';
                }

                curFileChanges.added += `${os.EOL}${row.substring(1)}`;
            }

            if (isRemoved) {
                if (!curFileChanges.removed) {
                    curFileChanges.removed = '';
                }

                curFileChanges.removed += `${os.EOL}${row.substring(1)}`;
            }
        }
    }

    return [ filesChanges, changesCounter, date, name, mail ];
}

const enum GIT_CONFIG_NAMES {
    path = 'webhook.path',
    port = 'webhook.port',
    hostname = 'webhook.hostname',
    enabled = 'webhook.enabled',
    url = 'webhook.url',
}

async function getWebhookGitConfig() {
    const configs = await _execGitCmd('git config --list');
    const setConfigs = OPTIONS.webhookOptions || {};
    const configsRows = configs.split(os.EOL);

    for (const configRow of configsRows) {
        if (!configRow.startsWith('webhook.')) {
            continue;
        }

        const [paramName, paramValue] = configRow.split('=') as [ paramName: GIT_CONFIG_NAMES, paramValue: string ];

        switch (paramName) {
            case GIT_CONFIG_NAMES.enabled: {
                if (paramValue === 'true') {
                    setConfigs.enabled = true;
                }
                else {
                    return;
                }

                break;
            }
            case GIT_CONFIG_NAMES.url: {
                const url = new URL(paramValue);
                let port = url.port;

                if (!port) {
                    if (url.protocol === 'https:') {
                        port = '443';
                    }

                    if (url.protocol === 'http:') {
                        port = '80';
                    }
                }

                setConfigs.port = port;
                setConfigs.hostname = url.hostname;
                setConfigs.path = url.pathname;

                return;
            }
            case GIT_CONFIG_NAMES.hostname: {
                setConfigs.hostname = paramValue; break;
            }
            case GIT_CONFIG_NAMES.port: {
                setConfigs.port = paramValue; break;
            }
            case GIT_CONFIG_NAMES.path: {
                setConfigs.path = paramValue; break;
            }
        }

        if (!OPTIONS.webhookOptions) {
            OPTIONS.webhookOptions = setConfigs;
        }
    }
}

async function _execGitCmd(cmd: string): Promise<string> {
    const  {
        stdout,
        stderr,
    } = await exec(cmd);

    if (stderr){
        throw new Error(`[pre-push] Error while trying to execute ${cmd}: ${stderr}`);
    }

    return stdout.trim();
}

async function fetch(sendData) {
    if (sendData) {
        if (typeof sendData !== 'string') {
            sendData = JSON.stringify(sendData);
        }
    }
    else {
        sendData = '';
    }

    return new Promise((resolve, reject) => {
        const webhookOptions = OPTIONS.webhookOptions;

        if (!webhookOptions) {
            reject('No options');

            return;
        }

        const {
            hostname,
            port,
            path,
            customHeaders = {},
        } = webhookOptions;
        const reqOptions = {
            hostname,
            port,
            path,
            method: 'POST',
            headers: {
                ...customHeaders,
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': Buffer.byteLength(sendData),
            },
        };
        const req = http.request(reqOptions, res => {
            res.setEncoding('utf8');

            // const headerDate = res.headers && res.headers.date ? res.headers.date : 'no response date';
            // const data = [];

            // res.on('data', chunk => {
            //     data.push(chunk);
            // });
            res.on('end', () => {
                // const dataJson = JSON.parse(Buffer.concat(data).toString());
                resolve(sendData);
            });
        })

        // Write data to request body
        req.write(sendData);
        req.end();
    });
}


function handleGitBranchCommandError(e){
    console.log('something bad happened when trying to read the repository branches using the "git branch" command');
    console.log('this is script is intended to be run as a git hook inside a git repository. Are you sure you\'re calling it properly?');
    console.log('the error provided by the "git branch" invocation was the following:');
    console.log(e.getMessage() );
    console.log('----');
    console.log('Your commit will be rejected. This script will terminate.');
    process.exit(1);
}

function handleBadBranchName(){

    console.log('There is something wrong with your branch name');
    console.log('branch names in this project must adhere to this contract:' + OPTIONS.branchRegEx);
    console.log('they must start either with the "feature" or "hotfix" word, followed by a "/" char and then a Jira valid issue identifier, followed by a dash');
    console.log('Your commit will be rejected. You should rename your branch to a valid name, for instance, you could run a command like the following to rename your branch:');
    console.log('git branch -m feature/HP-1201-some-killer-feature');
    console.log('if you thing there is something wrong with this message, or that your branch name is not being validated properly, check the commit-msg git hook');
    process.exit(1);
}

function handleBadCommitMessage(){

    console.log('There is something wrong with your commit message');
    console.log('it should start with a valid Jira issue code, followed by a dash, thus adhering to this contract:' + OPTIONS.commitMsgRegEx);
    console.log('your commit will be rejected. Please re-commit your work again with a proper commit message.');
    console.log('if you thing there is something wrong with this message, or that your commit message is not being validated properly, check the commit-msg git hook');
    process.exit(1);
}

function hookCleanup(){
    setTimeout(() => {
        console.log('This is a timeout message from your commit-msg git hook. If you see this, something bad happened in your pre-commit hook, and it absolutely did not work as expected.');
        console.log(' Your commit will be rejected. Please read any previous error message related to your commit message, and/or check the commit-msg git hook script.');
        console.log(' You can find more info in this link: https://git-scm.com/book/uz/v2/Customizing-Git-An-Example-Git-Enforced-Policy');
        process.exit(1);
    }, OPTIONS.timeout);
}