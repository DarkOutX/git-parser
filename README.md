
Function createReport builds changelog by commit history of your repository.

## Parameters:
 - **pathToRepository**: *string*  
   path to root of repository where you would run 'git log'
 - **type**: *REPORT_TYPE* | *number*  
   - 1 - JSON. Will generate changelog.json  
   - 2 - Markdown. Will generate formatted changelog.md  
   - 3 - Console. Will output formatted changelog in console
 - **options *(optional)***: *Object* | *void* 
   - options.**from?**: *number* | *void*  
     timestamp, start of the interval you want to get changelog 
   - options.**to?**: *number* | *void*  
     timestamp end of the interval you want to get changelog 
   - options.**isNeedAuthors?**: *boolean* | *void*  
     add "by %COMMIT_AUTHOR%" at the end of messages or not *(true by default)*
   - options.**skipTypes?**: *Object* | *void*  
     flags to skip some types of commits
     - options.skipTypes.**feat?**: *true* | *void* - skip refactoring commits
     - options.skipTypes.**fix?**: *true* | *void* - skip fixing commits
     - options.skipTypes.**doc?**: *true* | *void* - skip documentation commits *(true by default)*
     - options.skipTypes.**tests?**: *true* | *void* - skip test commits *(true by default)*
     - options.skipTypes.**ref?**: *true* | *void* - skip refactoring commits *(true by default)*
     - options.skipTypes.**dep?**: *true* | *void* - skip dependencies commits *(true by default)*

## Commit Style Guide

### Commit structure:
```
*commit_type*(*module*): *message*
```

### Structure description:

 - **commit_type** - type of commit, the most important part
   - **feat** - when you add some new functionality
   - **fix** - when you fixed some errors
   - **doc** - when you add some documentation
   - **tests** - when you added some tests
   - **ref** - when you refactored some old code
   - **dep** - when you updates dependencies
 - **module** - name of affected module  
  It's better to use same names here for each module, but you can write here any strings, changelog will be grouped by it
 - **message** - description of your commit. Here your can write any text, but it will be much more useful 
  if you will think in advance and write here understandable messages for those who will read changelog 

## Additional:

The best way to use this function is to add git hook commit-msg
To do this, create file **.git/hooks/commit-msg** containing text below:

```bash
#!/bin/sh

PATTERN='^(feat|fix|doc|tests|ref|dep)\([^:()]+\)\:\ .+$'
COMMIT_MSG=`cat $1`

if ! [[ $COMMIT_MSG =~ $PATTERN ]]; then
	echo ""
	echo -e "\e[1m\e[4m\e[31mWrong commit message\e[0m"
	echo ""
	echo -e "\e[1mCorrect structure:"
	echo -e "\e[33m*commit_type*\e[0m(\e[36m*module*\e[0m): *message*"
	echo ""
	echo -e "\e[1mValid commit types:\e[0m"
	echo -e "\e[33mfeat\e[0m | \e[33mfix\e[0m | \e[33mdoc\e[0m | \e[33mtests\e[0m | \e[33mref\e[0m | \e[33mdep\e[0m"
	echo ""
	echo -e "\e[1mExamples:\e[0m"
	echo -e "ref(global): removed deprecated functions"
	echo -e "docs(MyModule): described main methods"
	echo -e "feat(Core): added dataType param support"
	echo ""
	echo -e "To skip this validation use \e[33m--no-verify\e[0m"
	echo ""
	exit 1
fi

exit 0
```

Or [just copy it](./hooks/commit-msg)