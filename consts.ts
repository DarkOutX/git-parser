
export const enum COMMIT_TYPE {
    FEATURE = 'feat',
    REFACTORING = 'ref',
    FIX = 'fix',
    DOC = 'doc',
    TEST = 'tests',
    DEPENDENCIES = 'deps',
}

export const COMMIT_TYPE_TITLE: Record<COMMIT_TYPE, string> = {
    [COMMIT_TYPE.FEATURE]: 'FEATURES',
    [COMMIT_TYPE.REFACTORING]: 'REFACTORING',
    [COMMIT_TYPE.FIX]: 'FIXES',
    [COMMIT_TYPE.DOC]: 'DOCUMENTATION',
    [COMMIT_TYPE.TEST]: 'TESTS',
    [COMMIT_TYPE.DEPENDENCIES]: "DEPENDENCIES",
}

export const enum REPORT_TYPE {
    JSON = 1,
    MD = 2,
    CONSOLE = 3,
}

export const enum TEXT_STYLE {
    MAIN_TITLE = 1,
    SUB_TITLE = 2,
    DATE = 3,
    MODULE = 4,
    MODULE_NAME = 5,
    TEXT = 6,
    AUTHOR = 7,
}