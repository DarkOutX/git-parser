
export namespace IHook {

    export interface UserData {
        name: string;
        mail: string;
    }

    interface FileChanges_moved {
        isMoved: true;
        oldPath: string;
        newPath: string;
    }

    interface FileChanges_deleted {
        isDeletedFile: true;
    }

    interface FileChanges_newFile {
        isNewFile: true;
        /** Depends on config */
        added?: string;
    }

    interface FileChanges_edited {
        added?: string;
        removed?: string;
    }

    export type FileChanges_strict = FileChanges_edited | FileChanges_deleted | FileChanges_moved | FileChanges_newFile;

    export interface FileChanges {
        added?: string;
        removed?: string;

        isNewFile?: true;
        isDeletedFile?: true;

        isMoved?: true;
        oldPath?: string;
        newPath?: string;
    }

    export interface CommitData_min {
        /** Hash of commit */
        hash: string;
        /** Commit message */
        message: string;
        /** Currently selected branch */
        branchName: string;
        /** Date of commit as ISO */
        date?: string;
        /** Date of commit as timestamp */
        timestamp?: number;
        /** Info about author of commit */
        user?: UserData;
        /** Map, where key is filepath, and value is an object with updates */
        changes?: Record<string /* filepath */, FileChanges_strict>;
        /** Amount of affected files */
        changesAmount?: number;
    }

    export interface CommitData extends CommitData_min{
        /** Date of commit as ISO */
        date: string;
        /** Date of commit as timestamp */
        timestamp: number;
        /** Info about author of commit */
        user: UserData;
        /** Map, where key is filepath, and value is an object with updates */
        changes: Record<string /* filepath */, FileChanges_strict>;
        /** Amount of affected files */
        changesAmount: number;
    }

    export interface Options {
        /** Is need to validate branch name */
        isCheckBranchName?: boolean;
        /** RegEx to validate branch name */
        branchRegEx?: RegExp;

        /** Is need to validate commit messages */
        isCheckCommitMsg?: boolean;
        /** RegEx to validate commit messages */
        commitMsgRegEx?: RegExp;

        /** Options to send commit-info to webhook. Some of them can be loaded from git config */
        webhookOptions?: WebhookOptions,

        /** Limit of hook execute duration */
        timeout?: number;
        /** Prevents real push even if success */
        isDebug?: boolean;
    }

    export interface WebhookOptionsBase {
        /** Use hook or not */
        enabled?: boolean;
        /** Send edited file content or not */
        isGetFileChanges?: boolean;
        /** Send new file content or not */
        isSendNewFileContent?: boolean;
        /** localhost / 127.0.0.1 / http://mySite.com */
        hostname?: string;
        /** Port number */
        port?: number | string;
        /** Part of URL after hostname, including slash (/) at the end */
        path?: string;

        /** Not implemented */
        credentials?: {
            login: string;
            password: string;
        },
        /** Map of HeaderName:HeaderValue */
        customHeaders?: Record<string, string>;
    }

    export interface WebhookOptions_enabled extends WebhookOptionsBase {
        enabled: true;
        hostname: string;
        port: number | string;
        path: string;
    }

    export interface WebhookOptions_disabled extends WebhookOptionsBase {
        enabled?: false | void;
        hostname?: string  | void;
        port?: number | string | void;
        path?: string | void;
    }

    type WebhookOptions = WebhookOptions_enabled | WebhookOptions_disabled;
}