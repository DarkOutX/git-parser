import fs from 'fs';
import {gitToJs} from 'git-parse';
import {TEXT_MODS} from './const_styles';
import {COMMIT_TYPE, COMMIT_TYPE_TITLE, REPORT_TYPE, TEXT_STYLE} from './consts';
import type {IGitParser, IMessageParsed, IOptions, TMessages} from './types';

export const COMMIT_TYPES_LIST: COMMIT_TYPE[] = [
    COMMIT_TYPE.FEATURE,
    COMMIT_TYPE.REFACTORING,
    COMMIT_TYPE.FIX,
    COMMIT_TYPE.DOC,
    COMMIT_TYPE.TEST,
    COMMIT_TYPE.DEPENDENCIES,
];

const MIN_INTERVAL = 1000 * 60 * 60 * 24 * 30;
const GLOBAL_MODULE_NAME = 'global';

export function parseCommitMsg(msg: string): IMessageParsed {
    const i_textStart = msg.indexOf(':') + 1;
    let i_moduleStart = msg.indexOf('(');
    let i_moduleEnd = msg.indexOf(')');

    let isModuleExists = true;

    if (i_moduleStart === -1) {
        i_moduleStart = msg.indexOf(':');
        i_moduleEnd = i_moduleStart;

        isModuleExists = false;
    }

    const type = msg.slice(0, i_moduleStart) as COMMIT_TYPE;
    const module = isModuleExists ? msg.slice(i_moduleStart + 1, i_moduleEnd) : GLOBAL_MODULE_NAME;
    const text = msg.slice(i_textStart + 1, msg.length);

    return {
        type,
        module,
        text,
    };
}

function _collectMessages(commits: IGitParser.GitCommit[], type: REPORT_TYPE, options: IOptions = {}): TMessages {
    const {
        from = Date.now() - MIN_INTERVAL,
        to = Date.now(),
        isNeedAuthors = true,
        skipTypes = {
            deps: true,
            ref: true,
            tests: true,
        },
    } = options;
    const commitsAmount = commits.length;
    const messages: TMessages = {};

    for (let i = 0; i < commitsAmount; i++) {
        const commit = commits[i];
        const {
            date: dateStr,
            authorName,
            message,
        } = commit;
        const date = (new Date(dateStr)).getTime();

        if (date < from) {
            continue;
        }

        if (date > to) {
            break;
        }

        const {
            type: commitType,
            module,
            text: commitText,
        } = parseCommitMsg(message);

        if (skipTypes[commitType]) {
            continue;
        }

        if (!COMMIT_TYPES_LIST.includes(commitType)) {
            continue;
        }

        if (!messages[commitType]) {
            messages[commitType] = {};
        }

        if (!messages[commitType]![module]) {
            messages[commitType]![module] = [];
        }

        const textMods = TEXT_MODS[type];

        const basePre = textMods[TEXT_STYLE.TEXT].pre;
        const basePost = textMods[TEXT_STYLE.TEXT].post;
        const authorPre = textMods[TEXT_STYLE.AUTHOR].pre;
        const authorPost = textMods[TEXT_STYLE.AUTHOR].post;

        const text = isNeedAuthors ? `${basePre}${commitText} by ${authorPre}${authorName}${authorPost}${basePost}` : `${basePre}${commitText}${basePost}`;

        messages[commitType]![module].push(text);
    }

    return messages;
}

function is0(num: number): string {
    if (num < 10) {
        return `0` + num;
    }
    else {
        return '' + num;
    }
}

function _formatDate(timestamp: number): string {
    const D = new Date(timestamp);
    let d = D.getDate();
    let m = D.getMonth() + 1;
    let y = D.getFullYear();

    return `${is0(d)}.${is0(m)}.${is0(y)}`;
}

export async function createReport(pathToRepository: string, type: REPORT_TYPE, options: IOptions = {}) {
    const {
        from = Date.now() - MIN_INTERVAL,
        to = Date.now(),
    } = options;
    const commits: IGitParser.GitCommit[] = await gitToJs(pathToRepository);
    const messages = _collectMessages(commits, type, options);

    if (type === REPORT_TYPE.JSON) {
        return fs.writeFileSync('changelog.json', JSON.stringify(messages, null, '\t'));
    }

    let reportTitle = TEXT_MODS[type][TEXT_STYLE.MAIN_TITLE].pre;

    if (type === REPORT_TYPE.CONSOLE) {
        reportTitle += '█▀▀ █ █ ▄▀█ █▄ █ █▀▀ █▀▀ █   █▀█ █▀▀\n';
        reportTitle += '█▄▄ █▀█ █▀█ █ ▀█ █▄█ ██▄ █▄▄ █▄█ █▄█';
    }
    else if (type === REPORT_TYPE.MD) {
        reportTitle += 'CHANGELOG';
    }

    reportTitle += TEXT_MODS[type][TEXT_STYLE.MAIN_TITLE].post;

    const dateModPre = TEXT_MODS[type][TEXT_STYLE.DATE].pre;
    const dateModPost = TEXT_MODS[type][TEXT_STYLE.DATE].post;

    reportTitle += `from ${dateModPre}${_formatDate(from)}${dateModPost} to ${dateModPre}${_formatDate(to)}${dateModPost}`;
    reportTitle += '\n';

    let reportBody = '';

    const typeModPre = TEXT_MODS[type][TEXT_STYLE.SUB_TITLE].pre;
    const typeModPost = TEXT_MODS[type][TEXT_STYLE.SUB_TITLE].post;

    const moduleModPre = TEXT_MODS[type][TEXT_STYLE.MODULE].pre;
    const moduleModPost = TEXT_MODS[type][TEXT_STYLE.MODULE].post;
    const moduleNameModPre = TEXT_MODS[type][TEXT_STYLE.MODULE_NAME].pre;
    const moduleNameModPost = TEXT_MODS[type][TEXT_STYLE.MODULE_NAME].post;

    for (let commitType in messages) {
        const curTypeModPre = TEXT_MODS[type][commitType].pre;
        const curTypeModPost = TEXT_MODS[type][commitType].post;

        reportBody += `${typeModPre}${curTypeModPre}${COMMIT_TYPE_TITLE[commitType]}${curTypeModPost}${typeModPost}`;

        for (let moduleName in messages[commitType]) {
            const moduleMessages = messages[commitType][moduleName];

            reportBody += `${moduleModPre}Module: ${moduleNameModPre}${moduleName}${moduleNameModPost}${moduleModPost}`;

            moduleMessages.forEach(msg => {
                reportBody += msg;
            });
        }
    }

    if (type === REPORT_TYPE.CONSOLE) {
        console.log(reportTitle + reportBody);
    }
    else if (type === REPORT_TYPE.MD) {
        fs.writeFileSync('./CHANGELOG.md', reportTitle + reportBody);
    }
}